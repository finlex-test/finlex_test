# README

### Setup

```
rails db:create
rails server
```

### Setup Docker
```
docker-compose build
docker-compose up
docker-compose run web rake db:create
open http://localhost:3000/
```
### Generate the Swagger JSON file

```
rake rswag:specs:swaggerize
open http://localhost:3000/api-docs
```

### See online demo
https://finlex-test.herokuapp.com/api-docs/index.html
