# frozen_string_literal: true

Rails.application.routes.draw do
  resources :customers do
    resources :contracts, controller: 'customer_contracts' do
      post :bulk_create, on: :collection
    end
  end
  resources :contracts, only: [] do
    post :bulk_create, on: :collection
  end

  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
end
