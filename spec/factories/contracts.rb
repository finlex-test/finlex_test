# frozen_string_literal: true

FactoryBot.define do
  factory :contract do
    association :customer
    start_date { Faker::Date.in_date_period }
    end_date { Faker::Date.in_date_period }
    expiry_date { Faker::Date.in_date_period }
    price { Faker::Commerce.price }
  end
end
