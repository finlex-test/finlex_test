# frozen_string_literal: true

require 'swagger_helper'

describe 'Customers API show endpoint' do
  path '/customers/{id}' do
    get 'Retrieves a customer' do
      tags 'Customers'
      produces 'application/json'
      parameter name: :id, in: :path, type: :string

      response '200', 'customer found' do
        schema type: :object,
               properties: {
                 name: { type: :string },
                 email: { type: :string },
                 address: { type: :string }
               },
               required: %w[name email address]

        let(:id) { create(:customer).id }
        before { id }

        run_test!
      end

      response '404', 'customer not found' do
        let(:id) { 'invalid' }
        run_test!
      end
    end
  end
end
