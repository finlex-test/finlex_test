# frozen_string_literal: true

require 'swagger_helper'

describe 'Customers API index endpoint' do
  path '/customers' do
    get 'Retrieves all customers' do
      tags 'Customers'
      produces 'application/json'
      response '200', :success do
        schema type: :array,
               items: {
                 type: :object,
                 properties: {
                   name: { type: :string },
                   email: { type: :string },
                   address: { type: :string }
                 }
               },
               required: %w[name email address]

        before { create(:customer) }

        run_test!
      end
    end
  end
end
