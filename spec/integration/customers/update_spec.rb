# frozen_string_literal: true

require 'swagger_helper'

describe 'Customers API update endpoint' do
  path '/customers/{id}' do
    put 'Updates a customer' do
      tags 'Customers'
      parameter name: :id, in: :path, type: :string
      parameter name: :customer, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string },
          email: { type: :string },
          address: { type: :string }
        },
        required: %w[name email address]
      }

      let(:customer) { { name: 'changed name', email: 'changed email', address: 'changed address' } }

      response '200', 'customer updated' do
        let(:id) { create(:customer).id }
        before { id }

        run_test!
      end

      response '404', 'customer not found' do
        let(:id) { 'invalid' }
        run_test!
      end
    end
  end
end
