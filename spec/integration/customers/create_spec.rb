# frozen_string_literal: true

require 'swagger_helper'

describe 'Customers API create endpoint' do
  path '/customers' do
    post 'Creates a customer' do
      tags 'Customers'
      parameter name: :customer, in: :body, schema: {
        type: :object,
        properties: {
          name: { type: :string },
          email: { type: :string },
          address: { type: :string }
        },
        required: %w[name email address]
      }

      response '201', 'customer created' do
        let(:customer) { { name: 'foo', email: 'bar@example.com', address: 'Frankfurt Max street 10' } }
        run_test!
      end

      response '422', 'invalid request' do
        let(:customer) { { address: 'foo' } }
        run_test!
      end
    end
  end
end
