# frozen_string_literal: true

require 'swagger_helper'

describe 'Customers API destroy endpoint' do
  path '/customers/{id}' do
    delete 'Deletes a customer' do
      tags 'Customers'
      parameter name: :id, in: :path, type: :string

      response '204', 'customer deleted' do
        let(:id) { create(:customer).id }

        before { id }

        it 'deletes customer' do |example|
          expect { submit_request(example.metadata) }.to change(Customer, :count).by(-1)
        end
      end

      response '404', 'customer not found' do
        let(:id) { 'invalid' }
        run_test!
      end
    end
  end
end
