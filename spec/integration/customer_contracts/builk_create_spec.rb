# frozen_string_literal: true

require 'swagger_helper'

describe 'Customer contracts API bulk create endpoint' do
  path '/customers/{customer_id}/contracts/bulk_create' do
    post 'Creates multiple customer contracts' do
      tags 'Customer contracts'
      parameter name: :customer_id, in: :path, type: :string
      parameter name: :contracts, in: :body, schema: {
        type: :array,
        items: {
          type: :object,
          properties: {
            start_date: { type: :string },
            end_date: { type: :string },
            expiry_date: { type: :string },
            price: { type: :number }
          }
        }
      }

      let!(:customer_id) { create(:customer).id }

      response '201', 'customer contract created' do
        let(:contracts) do
          [
            { start_date: 1.day.ago, end_date: 1.week.from_now, expiry_date: 1.year.from_now, price: 20.0 },
            { start_date: 2.days.ago, end_date: 2.weeks.from_now, expiry_date: 2.years.from_now, price: 30.0 }
          ]
        end

        run_test!
      end
    end
  end
end
