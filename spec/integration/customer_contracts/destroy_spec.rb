# frozen_string_literal: true

require 'swagger_helper'

describe 'Customer contracts API destroy endpoint' do
  path '/customers/{customer_id}/contracts/{id}' do
    delete 'Deletes a customer' do
      tags 'Customer contracts'
      parameter name: :id, in: :path, type: :string
      parameter name: :customer_id, in: :path, type: :string

      let!(:customer) { create(:customer) }
      let(:customer_id) { customer.id }

      response '204', 'customer deleted' do
        let!(:id) { create(:contract, customer: customer).id }

        it 'deletes customer contract' do |example|
          expect { submit_request(example.metadata) }.to change(customer.contracts, :count).by(-1)
        end
      end

      response '404', 'customer contract not found' do
        let(:id) { 'invalid' }

        run_test!
      end
    end
  end
end
