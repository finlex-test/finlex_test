# frozen_string_literal: true

require 'swagger_helper'

describe 'Customer contracts API update endpoint' do
  path '/customers/{customer_id}/contracts/{id}' do
    put 'Updates a customer contract' do
      tags 'Customer contracts'
      parameter name: :id, in: :path, type: :string
      parameter name: :customer_id, in: :path, type: :string
      parameter name: :contract, in: :body, schema: {
        type: :object,
        properties: {
          start_date: { type: :date },
          end_date: { type: :date },
          expiry_date: { type: :date },
          price: { type: :float }
        }
      }
      let!(:customer) { create(:customer) }
      let(:customer_id) { customer.id }
      let(:contract) do
        { start_date: 1.day.ago, end_date: 1.week.from_now, expiry_date: 1.year.from_now, price: 20.0 }
      end

      response '200', 'customer contract updated' do
        let!(:id) { create(:contract, customer: customer).id }

        run_test!
      end

      response '404', 'customer not found' do
        let(:id) { 'invalid' }

        run_test!
      end
    end
  end
end
