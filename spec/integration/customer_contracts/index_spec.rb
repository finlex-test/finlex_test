# frozen_string_literal: true

require 'swagger_helper'

describe 'Customers contracts API index endpoint' do
  path '/customers/{customer_id}/contracts' do
    get 'Retrieves all contracts of a customer' do
      tags 'Customer contracts'
      produces 'application/json'
      parameter name: :customer_id, in: :path, type: :string

      let(:customer) { create(:customer) }
      let(:customer_id) { customer.id }

      before do
        customer
        create(:contract, customer: customer)
      end

      response '200', :success do
        schema type: :array,
               items: {
                 type: :object,
                 properties: {
                   start_date: { type: :date },
                   end_date: { type: :date },
                   expiry_date: { type: :date },
                   price: { type: :float }
                 }
               },
               required: %w[start_date end_date expiry_date price]

        run_test!
      end
    end
  end
end
