# frozen_string_literal: true

require 'swagger_helper'

describe 'Customer contracts API show endpoint' do
  path '/customers/{customer_id}/contracts/{id}' do
    get 'Retrieves a customer contract' do
      tags 'Customer contracts'
      produces 'application/json'
      parameter name: :id, in: :path, type: :string
      parameter name: :customer_id, in: :path, type: :string

      let!(:customer) { create(:customer) }
      let(:customer_id) { customer.id }

      response '200', 'customer contract found' do
        schema type: :object,
               properties: {
                 start_date: { type: :date },
                 end_date: { type: :date },
                 expiry_date: { type: :date },
                 price: { type: :float }
               },
               required: %w[start_date end_date expiry_date price]

        let!(:id) { create(:contract, customer: customer).id }

        run_test!
      end

      response '404', 'customer not found' do
        let(:id) { 'invalid' }
        run_test!
      end
    end
  end
end
