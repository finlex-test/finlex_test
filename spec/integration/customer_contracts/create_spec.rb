# frozen_string_literal: true

require 'swagger_helper'

describe 'Customer contracts API create endpoint' do
  path '/customers/{customer_id}/contracts' do
    post 'Creates a customer contract' do
      tags 'Customer contracts'
      parameter name: :customer_id, in: :path, type: :string
      parameter name: :contract, in: :body, schema: {
        type: :object,
        properties: {
          start_date: { type: :date },
          end_date: { type: :date },
          expiry_date: { type: :date },
          price: { type: :float }
        }
      }

      let!(:customer_id) { create(:customer).id }

      response '201', 'customer contract created' do
        let(:contract) do
          { start_date: 1.day.ago, end_date: 1.week.from_now, expiry_date: 1.year.from_now, price: 20.0 }
        end

        run_test!
      end
    end
  end
end
