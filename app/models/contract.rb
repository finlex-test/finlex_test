# frozen_string_literal: true

class Contract
  include Mongoid::Document
  include Mongoid::Timestamps
  field :start_date, type: Date
  field :end_date, type: Date
  field :expiry_date, type: Date
  field :price, type: BigDecimal

  belongs_to :customer
end
