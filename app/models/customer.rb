# frozen_string_literal: true

class Customer
  include Mongoid::Document
  include Mongoid::Timestamps
  field :name, type: String
  field :email, type: String
  field :address, type: String

  has_many :contracts, dependent: :destroy

  validates :name, presence: true
end
