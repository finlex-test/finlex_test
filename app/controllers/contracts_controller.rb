# frozen_string_literal: true

class ContractsController < ApplicationController
  def bulk_create
    Contracts::BulkCreateService.call(params)
    json_response(nil, status: :created)
  end
end
