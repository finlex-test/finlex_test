# frozen_string_literal: true

module Response
  # @param object [WillPaginate::Collection|ActiveRecord_Relation|Array<ActiveRecordObject>] the object can be an
  # active_record object or collection of objects
  # @param status [Symbol] can be any standard status or http status number for instance 404
  def json_response(object, status: :ok)
    render json: object, status: status
  end
end
