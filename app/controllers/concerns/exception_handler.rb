# frozen_string_literal: true

module ExceptionHandler
  extend ActiveSupport::Concern

  included do
    rescue_from Mongoid::Errors::DocumentNotFound, Mongoid::Errors::InvalidFind do |e|
      json_response({ errors: [{ message: e.message }] }, status: :not_found)
    end

    rescue_from Errors::RecordInvalid do |e|
      json_response({ errors: [{ message: e.message }] }, status: :unprocessable_entity)
    end
  end
end
