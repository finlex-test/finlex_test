# frozen_string_literal: true

class CustomerContractsController < ApplicationController
  def index
    call_params = {
      filter: {
        contract: {
          customer_id: { eq: params[:customer_id] }
        }
      }
    }
    contracts = ContractsQuery.call(call_params, authorization_employee: nil)
    json_response(contracts)
  end

  def show
    contract = ContractQuery.call({ id: params[:id] }, authorization_employee: nil)
    json_response(contract)
  end

  def create
    result = Contracts::CreateService.call(params)

    if result.success?
      json_response(result.contract, status: :created)
    else
      json_response(result.contract.errors, status: result.error.key)
    end
  end

  def bulk_create
    Contracts::BulkCreateService.call(params)
    json_response(nil, status: :created)
  end

  def update
    result = Contracts::UpdateService.call(params)

    if result.success?
      json_response(result.contract)
    else
      json_response(result.contract.errors, status: result.error.key)
    end
  end

  def destroy
    Contracts::DestroyService.call(params)
  end

  private

  def customer
    @customer ||= Customer.find(params[:customer_id])
  end
end
