# frozen_string_literal: true

# Return value of Service#call
class ServiceResult < OpenStruct
  def failure!
    self._success = false
  end

  def failure?
    !success?
  end

  def success!
    self._success = true
  end

  def success?
    _success == true
  end
end
