# frozen_string_literal: true

module Contracts
  class UpdateService < Base
    def handle
      result.contract = contract
      failure!(:unprocessable_entity) unless result.contract.update(contract_params)
    end
  end
end
