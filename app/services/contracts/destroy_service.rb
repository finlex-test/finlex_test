# frozen_string_literal: true

module Contracts
  class DestroyService < Base
    def handle
      contract.destroy
    end
  end
end
