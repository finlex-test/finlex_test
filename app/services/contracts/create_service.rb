# frozen_string_literal: true

module Contracts
  class CreateService < Base
    def handle
      result.contract = Contract.new(contract_params)
      failure!(:unprocessable_entity) unless result.contract.save
    end
  end
end
