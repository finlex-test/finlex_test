# frozen_string_literal: true

module Contracts
  class BulkCreateService < Base
    def handle
      contracts = Contract.collection.insert_many(bulk_contracts_params)
      result.contracts = Contract.find(contracts.inserted_ids)
    end

    private

    def bulk_contracts_params
      params['_json'].map do |contract|
        contract_params = contract.permit(PERMITTED_PROPERTIES).as_json
        !contract_params.key?(:customer_id) || contract_params.merge!(customer_id: params[:customer_id])
        contract_params
      end
    end
  end
end
