# frozen_string_literal: true

module Contracts
  class Base < Service
    PERMITTED_PROPERTIES = %i[start_date end_date expiry_date price].freeze

    private

    def contract
      @contract ||= Contract.find(params[:id])
    end

    def contract_params
      params.require(:customer_contract).permit(PERMITTED_PROPERTIES).merge(customer_id: params[:customer_id])
    end
  end
end
