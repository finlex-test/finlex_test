# frozen_string_literal: true

# Base class for all services
class Service
  # This is just a quality of life method that enables to shorten
  # Service.new(args).call to Service.call(args).
  #
  def self.call(params)
    new(params).call
  end

  def initialize(params)
    self.params = params
    self._failure = false
    self.result = ServiceResult.new
  end

  def call
    handle
    result.success! unless failure?

    result
  end

  def handle
    raise NotImplementedError
  end

  def call!
    call
    raise 'Service call failed' if result.failure?

    result
  end

  private

  attr_accessor :_failure, :params, :result

  def failure!(error_key = nil, options = {})
    result.error = interpret_error(error_key, options)
    self._failure = true
  end

  def failure?
    _failure == true
  end

  def error_message_generator(error_key, options)
    case error_key
    when :unauthorized
      error_hash(:unauthorized, '401', options[:message], options[:label])
    when :unprocessable_entity
      error_hash(:unprocessable_entity, '422', options[:message], options[:label])
    when :failed_dependency
      error_hash(:failed_dependency, '424', options[:message], options[:label])
    else
      error_hash(:internal_server_error, '500', options[:message], options[:label])
    end
  end

  def error_hash(key, code, message, label)
    Hashie::Mash.new(
      code: code, key: key,
      message: message || I18n.t("services.errors.#{key}"),
      label: label
    )
  end
end
