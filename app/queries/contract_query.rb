# frozen_string_literal: true

class ContractQuery < RecordQuery
  private

  def model_class
    Contract
  end
end
