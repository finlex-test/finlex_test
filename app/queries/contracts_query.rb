# frozen_string_literal: true

class ContractsQuery < CollectionQuery
  private

  def handle(params)
    scoped = authorize(initial_scope)
    # scoped = search(scoped, params[:search])
    scoped = filter(scoped, params[:filter])
    # scoped = sort(scoped, params[:sort_type], params[:sort_direction])
    # paginate(scoped, params[:page])
  end

  def model_class
    Contract
  end

  def filter(scoped, params_filter)
    return scoped if params_filter.blank?

    filters = resolve_filters(params_filter)
    scoped = filter_by(scoped, filters[:contract_customer_id_eq])
  #   scoped = filter_by(scoped, filters[:consultant_id_eq])
  #   scoped = filter_by(scoped, filters[:voucher_event_date_ge])
  #   scoped = filter_by(scoped, filters[:voucher_event_date_le])
  #   scoped = filter_by_event(scoped, filters[:voucher_event_type_eq])
  #   filter_by_branch(scoped, filters[:branch_id_eq])
  end
end
