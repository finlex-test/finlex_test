# frozen_string_literal: true

class RecordQuery < ApplicationQuery
  private

  def handle(params)
    scoped = authorize(initial_scope)
    find(scoped, params[:id])
  end

  def find(scoped, id)
    # authorized_record = scoped.find_by(id: id)
    # return authorized_record if authorized_record.present?

    record_in_db = model_class.find(id) # if the record does not exist, will trigger 404 exception
    # raise Pundit::NotAuthorizedError, query: 'read?', record: record_in_db
  end
end
