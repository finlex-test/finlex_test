# frozen_string_literal: true

class CollectionQuery < ApplicationQuery
  WHITELISTED_SORT_DIRECTIONS = %w[asc desc].freeze
  FILTER_OPERANDS_MAPPING = {
    'eq' => 'IN',
    'ge' => '>=',
    'le' => '<='
  }.freeze

  private

  def paginate(scoped, page)
    per_page = page && page['size'] ? page['size'] : 25
    page_number = page && page['number'] ? page['number'] : nil
    scoped.paginate(page: page_number, per_page: per_page)
  end

  def sort(scoped, sort_type, sort_direction)
    sanitized_sort_type = sanitize_sort_type(sort_type.presence || default_sort_type)
    sanitized_sort_direction = WHITELISTED_SORT_DIRECTIONS.include?(sort_direction) ? sort_direction : 'asc'

    scoped.order("#{sanitized_sort_type} #{sanitized_sort_direction}")
  end

  def sanitize_sort_type(_sort_type)
    raise NotImplementedError
  end

  def default_sort_type
    'id'
  end

  # Adds ActiveRecord includes to the query.
  # The format of includes should be exactly the same as ActiveRecord includes method
  # https://apidock.com/rails/ActiveRecord/QueryMethods/includes
  def add_includes(scoped, includes)
    return scoped if includes.blank?

    scoped.includes(includes)
  end

  # change the dot separated string to a nested hash
  # Exp.1 rights.action.resource_type => [{"rights"=>{"action"=>"resource_type"}}]
  # Exp.2 person,rights.action.resource_type => ["person", {"rights"=>{"action"=>"resource_type"}}]
  def hashified_include_query_string(include_param)
    return if include_param.blank?

    include_param.split(',').map do |single_include|
      association = single_include.split('.').reverse.inject { |a, n| { n => a } }
      next unless association_exists?(association)

      association
    end
  end

  def association_exists?(association)
    association = association.class == String ? association : association.keys.first
    model_class.reflect_on_association(association)
  end

  def resolve_filters(filters)
    results = {}
    filters.each do |type, fields|
      next if fields.nil?

      fields.each do |field, operands|
        next if operands.nil?

        operands.each do |operand, value|
          filter = "#{type} #{field} #{operand}".parameterize.underscore.to_sym
          results[filter] = {
            resource_type: map_element(type, 'resource_type'),
            field_name: field,
            operand: operand,
            value: value&.split(',')
          }
        end
      end
    end

    results
  end

  def filter_by(scoped, filter)
    return scoped if filter.blank?

    if filter[:resource_type].to_s.include?('.')
      query_hash = build_query_hash(filter)
      scoped.joins(query_hash[:joins]).where(query_hash[:conditions])
    else
      field_name = map_element(filter[:field_name], 'field_name')
      return scoped unless model_class.attribute_names.include?(field_name.to_s)

      # condition = "#{field_name} #{FILTER_OPERANDS_MAPPING[filter[:operand]]} (?)"
      scoped.where("#{field_name}": filter[:value].last)
    end
  end

  def build_query_hash(filter)
    resource_types = filter[:resource_type].split('.').drop(1)

    joins = {}
    resource_types.reduce(joins) { |key, value| key[value] = {} }

    conditions = {}
    pluralized_types = resource_types.map(&:pluralize)

    pluralized_types.reduce(conditions) do |key, value|
      key[value] = value == pluralized_types.last ? { filter[:field_name] => filter[:value] } : {}
    end
    { joins: joins, conditions: conditions }
  end

  def map_element(element_value, element_name)
    mapping_method = (element_name + '_mapping').to_sym
    return element_value unless self.class.respond_to? mapping_method

    self.class.public_send(mapping_method)[element_value] || element_value
  end

  def map_value(field_name, value)
    return value unless model_class.type_for_attribute(field_name).type == :boolean

    value.map { |val| val.to_s == 'true' }
  end
end
