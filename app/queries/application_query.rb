# frozen_string_literal: true

class ApplicationQuery
  def self.call(params, options)
    new(options).call(params)
  end

  def initialize(initial_scope: nil, authorization_disabled: false, authorization_employee: nil)
    self.initial_scope = initial_scope || unscoped
    # self.authorization_disabled = authorization_disabled
    # self.authorization_employee = authorization_employee
  end

  def call(params)
    handle(params)
    # result = handle(params)
    # scope_authorized_check!
    # result
  end

  private

  attr_accessor :authorization_disabled, :authorization_employee, :initial_scope

  # def scope_authorized_check!
  #   raise Pundit::NotAuthorizedError, 'Initial scope can not be used!' if @authorized_scope_is_being_used.nil?
  # end

  def handle(_options)
    raise NotImplementedError
  end

  def model_class
    raise NotImplementedError
  end

  def unscoped
    model_class.all
  end

  def authorize(scoped)
    # @authorized_scope_is_being_used = true
    # return scoped if authorization_disabled
    #
    # "#{scoped.klass}Policy::Scope".constantize.new(authorization_employee, scoped).resolve

    scoped
  end

  # def authorized_scope_is_being_used?
  #   @authorized_scope_is_being_used == true
  # end
end
