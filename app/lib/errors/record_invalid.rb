# frozen_string_literal: true

module Errors
  class RecordInvalid < StandardError; end
end
